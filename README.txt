
// $Header$

mobit (MobilizeIt or MobileTheme) is a module that allows users to choose a theme for use with mobile devices.
it has 2 parts. Part one allows the administrator to define one or more
of the installed themes as themes appropriate for use with mobile devices, meaning not much images, no
javascript, simple layout.
among those themes the user is able to select a theme as his mobile
theme. whenever the user accesses the site with a mobile device
(identified by the UA profile) this mobile theme will be used.
so the user default theme for web access will not change and the
web-browsing experience will be as good as usual.
i would like to share this module to get some feedback and new ideas
for enhancements.
as mobile devices get more and more popular i think this is an
interesting area for enhancements.

the code allows to switch to on access to an external rendering system.


